import { Component, OnInit, Input, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import * as _ from "lodash";
import { ConsoantesService} from './consoantes.service';
import {SpeechRecognitionService} from '../speech-recognition/speech-recognition.service';

@Component({
  selector: 'app-consoantes',
  templateUrl: './consoantes.component.html',
  styleUrls: ['./consoantes.component.css']
})
export class ConsoantesComponent implements OnInit, OnDestroy  {


  @Input()
  prop: String;
  letraDigitada: String;

  showSearchButton: boolean;
  speechData: String;
  maxLength:String;

  autoActivate = false;
  result:String="";

  @ViewChild('consoantesWord') private consoantesWord: ElementRef

  constructor(private consoantesService: ConsoantesService,
    private speechRecognitionService: SpeechRecognitionService) {

    this.showSearchButton = true;
    this.speechData = "";

    this.maxLength = "1";
  }

  ngOnInit() {
    this.prop = this.consoantesService.consonantesStart();
    this.consoantesWord.nativeElement.focus();
    this.result = "";
    if(this.autoActivate){
      setTimeout(() => {
        this.activateSpeechSearchMovie();
       }, 100);
    }
  }

  ngOnDestroy() {
    this.speechRecognitionService.DestroySpeechObject();
  }


  consonantes(letra: String){
    if(this.prop.toLowerCase() == letra.toLowerCase()){
      this.speechData = "";
      console.log('Acertou');
      this.prop = "Acertou"
      this.autoActivate = true;
      this.result = "true";

      setTimeout(() => {
       this.ngOnInit();
      }, 200);
    }else{

      this.speechData = "";
      console.log('Letra Errada');
      this.prop = "Errou"
      this.autoActivate = true;
      this.result = "false";

      setTimeout(() => {
        this.ngOnInit();
       }, 200);
    }
  }

  onKey(event: any) { // without type info
    var x = event.key;

    setTimeout(() => {
      this.consonantes(x);
     }, 500);
  }

  activateSpeechSearchMovie(): void {
    this.consoantesWord.nativeElement.focus();
    this.showSearchButton = false;
    var variavel;

    this.speechRecognitionService.record()
        .subscribe(
        //listener
        (value) => {

          if(value[0] == "é"){
            variavel = "e";
          }if(value[0] == "o" || value[0] == "ou"){
            variavel = "o";
          }if(value[0] == "u"){
            variavel = "u";
          }if(value[0] == "a"){
            variavel = "a";
          }if(value[0] == "e" || value[0] == "i"){
            variavel = "i";
          }else{
            variavel = value[0];
          }
            this.speechData = variavel;
            console.log(value);
            this.consonantes(this.speechData);
        },
        //errror
        (err) => {
            console.log(err);
            if (err.error == "no-speech") {
                console.log("--restatring service--");
                this.activateSpeechSearchMovie();
            }
        },
        //completion
        () => {
            this.showSearchButton = true;
            console.log("--complete--");
            this.activateSpeechSearchMovie();
        });
}


}
