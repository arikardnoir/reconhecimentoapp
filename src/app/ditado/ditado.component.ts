import { Component, OnInit, Input, OnDestroy, ElementRef, ViewChild  } from '@angular/core';
import * as _ from "lodash";

import {DitadoService} from './ditado.service';
import {SpeechRecognitionService} from '../speech-recognition/speech-recognition.service';


@Component({
  selector: 'app-ditado',
  templateUrl: './ditado.component.html',
  styleUrls: ['./ditado.component.css']
})
export class DitadoComponent implements OnInit {

  showSearchButton: boolean;
  speechData: String;
  word: string = '';

  @ViewChild('palavraWord') private palavraWord: ElementRef

  @Input()
  palavra:String;
  prop: String;
  atual: String;
  name:String;
  maxLength:string;
  image:String;
  pos:Number;

  newWord:String="";

  somador:any = 0;

  autoActivate = false;

  constructor(private ditadoService: DitadoService,
              private speechRecognitionService: SpeechRecognitionService) {

              this.showSearchButton = true;
              this.speechData = "";
  }

  ngOnInit() {

    var recebe = this.ditadoService.palavraStart();
    this.prop = recebe.name;
    this.palavra = "";
    this.image = recebe.img;
    this.maxLength = "" + (this.prop.length);

    console.log("hello");

    var input = document.getElementById('pal');
    this.setCaretPosition(input, 1);

    if(this.autoActivate){
      setTimeout(() => {
        this.activateSpeechSearchMovie();
       }, 1000);
    }
  }

  ngOnDestroy() {
    this.speechRecognitionService.DestroySpeechObject();
  }

  onKey(event: any) { // without type info
    var x = event.key;

    if(parseInt(this.maxLength) > this.word.length){
      this.word += x;
    }

  }

  palavraVerify(){

    if(this.word.toLowerCase() == this.prop.toLowerCase()){
      this.autoActivate = true;
      this.word = "";

      this.ngOnInit();
    }else{
      console.log(this.word);
      console.log('-----------');
      console.log(this.prop);
      console.log('Palavra Errada');
      this.autoActivate = true;
      this.word = "";
    }

  }

  setCaretPosition(ctrl, pos) {
    // Modern browsers
    if (ctrl.setSelectionRange) {
      ctrl.focus();
      ctrl.setSelectionRange(pos, pos);

    // IE8 and below
    } else if (ctrl.createTextRange) {
      var range = ctrl.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  }


  activateSpeechSearchMovie(): void {
    this.palavraWord.nativeElement.focus();
    this.showSearchButton = false;
    var variavel;

    this.speechRecognitionService.record()
        .subscribe(
        //listener
        (value) => {
            this.speechData = value[0];

            if(value[0] == "é"){
              variavel = "e";
            }else if(value == "ele" || value == "el"){
              variavel = "l";
            }else if(value == "PM" || value == "em"){
              variavel = "m";
            }
            else if(value == "ca" || value == "cá"){
              variavel = "k";
            }
            else if(value == "ze" || value == "zé"){
              variavel = "z";
            }
            else if(value == "ess" || value == "esse" || value == "eci"){
              variavel = "s";
            }else if(value == "ver" || value == "ve" || value == "vê"){
              variavel = "v";
            }else if(value == "te" || value == "ter"){
              variavel = "t";
            }else if(value == "ser" || value == "se"){
              variavel = "c";
            }
            else{
              variavel = value[0];
            }
            //this.palavra = this.ditadoService.palavraChange(this.palavra,value[0]);

        if(parseInt(this.maxLength) > this.word.length){
          this.word += variavel;
          this.palavra = this.word;
          console.log(this.word);
        }

        this.activateSpeechSearchMovie();
        },
        //errror
        (err) => {
            console.log(err);
            if (err.error == "no-speech") {
                console.log("--restatring service--");
                this.activateSpeechSearchMovie();
            }
        },
        //completion
        () => {
            this.showSearchButton = true;
            console.log("--complete--");
            this.activateSpeechSearchMovie();
        });
}




}
