import { Router } from '@angular/router';
import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import * as _ from "lodash";

interface IWindow extends Window {
    webkitSpeechRecognition: any;
    SpeechRecognition: any;
}



@Injectable()
export class DitadoService{

  speechRecognition: any;

  constructor(private zone: NgZone) {
  }

  palavraStart(){

    const letters = [
      { name: "Notebook",
        img: 'notebook.jpeg' },
      { name: "Jaleco",
      img: 'jaleco.jpeg' },
      { name: 'Mesa',
      img: 'mesa.png' },
      { name: 'Caderno',
      img: 'caderno.jpeg' },
      { name: 'Escola',
      img: 'escola.jpeg' },
      { name: 'Bola',
      img: 'bola.jpeg' },
      { name: 'Caneta',
      img: 'caneta.jpeg' },
      { name: 'Copo',
      img: 'copo.jpeg' },
      { name: 'Janela',
      img: 'janela.jpeg' },
      { name: 'Porta',
      img: 'porta.jpeg' }
    ];

    var rand = letters[Math.floor(Math.random() * letters.length)];

    return rand;
  }

  palavraSync(palavra: String){

    var random = Math.floor(Math.random() * palavra.length)

    for (let index = 0; index < palavra.length; index++) {

      if (index % 2 !== 0){

        var texto = palavra[index];
        palavra = palavra.replace(texto," ");

      }

    }

    return palavra;
  }

  palavraChange(palavra: String, letra: String){

    for (let index = 0; index < palavra.length; index++) {

      if (palavra[index] == " "){

        var texto = palavra[index];
        palavra = palavra.replace(texto, letra[0]);

        return palavra;
      }

    }

    return palavra;
  }

  blackSpacePosition(palavra: String){

    for (let index = 0; index < palavra.length; index++) {

      if (palavra[index] == " "){
       return index;
      }

    }

    return null;
  }


    record(): Observable<string> {

      return Observable.create(observer => {
          const { webkitSpeechRecognition }: IWindow = <IWindow>window;
          this.speechRecognition = new webkitSpeechRecognition();
          //this.speechRecognition = SpeechRecognition;
          this.speechRecognition.continuous = true;
          //this.speechRecognition.interimResults = true;
          this.speechRecognition.lang = 'en-us';
          this.speechRecognition.maxAlternatives = 1;

          this.speechRecognition.onresult = speech => {
              let term: string = "";
              if (speech.results) {
                  var result = speech.results[speech.resultIndex];
                  var transcript = result[0].transcript;
                  if (result.isFinal) {
                      if (result[0].confidence < 0.3) {
                          console.log("Unrecognized result - Please try again");
                      }
                      else {
                          term = _.trim(transcript);
                          console.log("Did you said? -> " + term + " , If not then say something else...");
                      }
                  }
              }
              this.zone.run(() => {
                  observer.next(term);
              });
          };

          this.speechRecognition.onerror = error => {
              observer.error(error);
          };

          this.speechRecognition.onend = () => {
              observer.complete();
          };

          this.speechRecognition.start();
          console.log("Say something - We are listening !!!");
      });
  }

  DestroySpeechObject() {
      if (this.speechRecognition)
          this.speechRecognition.stop();
  }

}

