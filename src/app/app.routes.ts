import { Routes,Router } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { HomeComponent } from './home/home.component';
import { AlfabetoComponent } from './alfabeto/alfabeto.component';
import { ConsoantesComponent } from './consoantes/consoantes.component';
import { DitadoComponent } from './ditado/ditado.component';
import { PalavrasComponent } from './palavras/palavras.component';
import { AboutComponent } from './about/about.component';

export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'alfabeto', component: AlfabetoComponent },
  { path: 'consoante', component: ConsoantesComponent },
  { path: 'ditado', component: DitadoComponent },
  { path: 'palavra', component: PalavrasComponent },
  { path: 'sobre', component: AboutComponent }
];

