import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { rootRouterConfig } from './app.routes';
import { AppComponent } from './app.component';
import { TesteComponent } from './teste/teste.component';
import { HomeComponent } from './home/home.component';
import { AlfabetoComponent } from './alfabeto/alfabeto.component';
import { ConsoantesComponent } from './consoantes/consoantes.component';
import { PalavrasComponent } from './palavras/palavras.component';
import { DitadoComponent } from './ditado/ditado.component';
import { AboutComponent } from './about/about.component';

import{ AlfabetoService } from './alfabeto/alfabeto.service';
import{ ConsoantesService } from './consoantes/consoantes.service';
import{ PalavrasService } from './palavras/palavras.service';
import{ DitadoService } from './ditado/ditado.service';
import { SpeechRecognitionService } from './speech-recognition/speech-recognition.service';


@NgModule({
  declarations: [
    AppComponent,
    TesteComponent,
    HomeComponent,
    AlfabetoComponent,
    ConsoantesComponent,
    PalavrasComponent,
    DitadoComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(rootRouterConfig)
  ],
  providers: [AlfabetoService,
              ConsoantesService,
              PalavrasService,
              DitadoService,
              SpeechRecognitionService],

  bootstrap: [AppComponent]
})
export class AppModule { }
