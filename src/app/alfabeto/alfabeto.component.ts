import { Component, OnInit, Input, SimpleChanges, SimpleChange, OnDestroy, ElementRef, ViewChild  } from '@angular/core';
import { SpeechRecognitionService } from '../speech-recognition/speech-recognition.service';
import { NgForOf } from '@angular/common';
import { forEach } from '@angular/router/src/utils/collection';

import{AlfabetoService} from './alfabeto.service';

@Component({
  selector: 'app-alfabeto',
  templateUrl: './alfabeto.component.html',
  styleUrls: ['./alfabeto.component.css']
})
export class AlfabetoComponent implements OnInit, OnDestroy {

  @Input()
  prop: String;
  letraDigitada:String;

  showSearchButton: boolean;
  speechData: string;

  autoActivate = false;
  result:String="";

  @ViewChild('alfabetoWord') private alfabetoWord: ElementRef

    constructor(private speechRecognitionService: SpeechRecognitionService,
                private alfabetoService: AlfabetoService) {
        this.showSearchButton = true;
        this.speechData = "";
    }

  ngOnInit() {
    this.prop = this.alfabetoService.alphabetStart();
    this.alfabetoWord.nativeElement.focus();
    this.result = "";

    if(this.autoActivate){
      setTimeout(() => {
        this.activateSpeechSearchMovie();
       }, 300);
    }
  }

  ngOnDestroy() {
      this.speechRecognitionService.DestroySpeechObject();
  }

  activateSpeechSearchMovie(): void {
    this.alfabetoWord.nativeElement.focus();
    this.showSearchButton = false;
    var variavel;

    this.speechRecognitionService.record()
        .subscribe(
        //listener
        (value) => {
          if(value[0] == "é"){
            variavel = "e";
          }else if(value == "ele" || value == "el"){
            variavel = "l";
          }
          else if(value == "ca" || value == "cá"){
            variavel = "k";
          }
          else if(value == "ze" || value == "zé"){
            variavel = "z";
          }
          else if(value == "ess" || value == "esse" || value == "eci"){
            variavel = "s";
          }else if(value == "ver" || value == "ve" || value == "vê"){
            variavel = "v";
          }else if(value == "te" || value == "ter"){
            variavel = "t";
          }else{
            variavel = value[0];
          }
            this.speechData = variavel;
            console.log(value);
            this.alphabet(this.speechData);
        },
        //errror
        (err) => {
            console.log(err);
            if (err.error == "no-speech") {
                console.log("--restatring service--");
                this.activateSpeechSearchMovie();
            }
        },
        //completion
        () => {
            this.showSearchButton = true;
            console.log("--complete--");
            this.activateSpeechSearchMovie();
        });
}

  alphabet(letra: String){

    if(this.prop.toLowerCase() == letra.toLowerCase()){
      this.speechData = "";
      console.log('Acertou');
      this.prop = "Acertou"
      this.autoActivate = true;
      this.result = "true";

      setTimeout(() => {
       this.ngOnInit();
      }, 500);

    }else{
      this.speechData = "";
      console.log('Letra Errada');
      this.prop = "Errou"
      this.autoActivate = true;
      this.result = "false";

      setTimeout(() => {
        this.ngOnInit();
       }, 500);
    }

  }

}
