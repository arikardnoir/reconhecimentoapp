import { Router } from '@angular/router';


export class AlfabetoService{

  constructor() { }

  alphabetStart(){

    const letters = [
      { name: 'A' },
      { name: 'B' },
      { name: 'C' },
      { name: 'D' },
      { name: 'E' },
      { name: 'F' },
      { name: 'G' },
      { name: 'H' },
      { name: 'J' },
      { name: 'K' },
      { name: 'L' },
      { name: 'M' },
      { name: 'N' },
      { name: 'O' },
      { name: 'P' },
      { name: 'Q' },
      { name: 'R' },
      { name: 'S' },
      { name: 'T' },
      { name: 'U' },
      { name: 'V' },
      { name: 'W' },
      { name: 'X' },
      { name: 'Y' },
      { name: 'Z' }
    ];

    var rand = letters[Math.floor(Math.random() * letters.length)];

    return rand.name;

  }

}
