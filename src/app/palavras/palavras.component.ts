import { Component, OnInit, Input, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import * as _ from "lodash";

import {PalavrasService} from './palavras.service';
import {SpeechRecognitionService} from '../speech-recognition/speech-recognition.service';

@Component({
  selector: 'app-palavras',
  templateUrl: './palavras.component.html',
  styleUrls: ['./palavras.component.css']
})
export class PalavrasComponent implements OnInit, OnDestroy {

  showSearchButton: boolean;
  speechData: String;

  @ViewChild('palavraWord') private palavraWord: ElementRef

  @Input()
  prop: String;
  atual: String;
  palavra:String;
  image:String;
  maxLength:String;
  pos:Number;

  constructor(private palavrasService: PalavrasService,
              private speechRecognitionService: SpeechRecognitionService) {

              this.showSearchButton = true;
              this.speechData = "";
  }

  ngOnInit() {

    var recebe = this.palavrasService.palavraStart();
    this.prop = recebe.name;
    this.image = recebe.img;
    this.palavra = this.palavrasService.palavraSync(this.prop);
    this.atual = this.palavra;
    this.maxLength = "" + this.palavra.length;
    this.pos = this.palavrasService.blackSpacePosition(this.palavra);

    console.log("hello");
    this.palavraWord.nativeElement.focus();

    var input = document.getElementById('pal');
    this.setCaretPosition(input, 1);

  }

  ngOnDestroy() {
    this.speechRecognitionService.DestroySpeechObject();
  }

  onKey(event: any) { // without type info
    var x = event.key;

    this.palavra = this.palavrasService.palavraChange(this.palavra,x);
  }

  palavraVerify(){

    if(this.palavra.toLowerCase() == this.prop.toLowerCase()){
      this.ngOnInit();
    }else{

      this.palavra = "";
    }

  }

  setCaretPosition(ctrl, pos) {
    // Modern browsers
    if (ctrl.setSelectionRange) {
      ctrl.focus();
      ctrl.setSelectionRange(pos, pos);

    // IE8 and below
    } else if (ctrl.createTextRange) {
      var range = ctrl.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  }


  activateSpeechSearchMovie(): void {
    this.palavraWord.nativeElement.focus();
    this.showSearchButton = false;
    var variavel;

    this.speechRecognitionService.record()
        .subscribe(
        //listener
        (value) => {

          if(value[0] == "é"){
            variavel = "e";
          }else if(value == "ele" || value == "el"){
            variavel = "l";
          }
          else if(value == "ca" || value == "cá"){
            variavel = "k";
          }
          else if(value == "ze" || value == "zé"){
            variavel = "z";
          }
          else if(value == "ess" || value == "esse" || value == "eci"){
            variavel = "s";
          }else if(value == "ver" || value == "ve" || value == "vê"){
            variavel = "v";
          }else if(value == "te" || value == "ter"){
            variavel = "t";
          }else{
            variavel = value[0];
          }

            this.speechData = variavel;

            this.palavra = this.palavrasService.palavraChange(this.palavra,variavel);
            console.log(value[0]);
            this.activateSpeechSearchMovie();
        },
        //errror
        (err) => {
            console.log(err);
            if (err.error == "no-speech") {
                console.log("--restatring service--");
                this.activateSpeechSearchMovie();
            }
        },
        //completion
        () => {
            this.showSearchButton = true;
            console.log("--complete--");
            this.activateSpeechSearchMovie();
        });
}




}
