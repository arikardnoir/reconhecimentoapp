var cnt=0, texts=[];
var $fclick = false;

$(".imawhat").each(function() {
  texts[cnt++]=$(this).text();
});

function fadeText() {
  if (cnt>=texts.length) { cnt=0; }
  $('.ima').html(texts[cnt++]);
  $('.ima')
    .fadeIn('fast').animate({opacity: 1.0}, 5000).fadeOut('fast',
     function() {
       return fadeText();
     }
  );
}

function toggleForm() {
  if ($fclick === true) {
    $(".contact, .head, .arm").toggleClass("active inactive");
  } else {
    $(".contact, .head, .arm").addClass("active");
    $fclick = true;
  }
}

$(".contactme, .arrow, .closer, .submit").on("click", toggleForm);

fadeText();
