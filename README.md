# ReconhecimentoApp

##Começando

Essas instruções farão com que você tenha uma cópia do projeto em execução na sua máquina local para fins de desenvolvimento e teste. Veja a implantação de notas sobre como implantar o projeto em um sistema ativo.
Pré-requisitos

Instalar ou verificar se o node está instalado em sua máquina. Para verificar basta executar os seguintes comandos no seu terminal de linha de comando.
•	Mostra a versão do node que está instalada em sua máquina

node -v

•	Mostra a versão do npm que está instalada em sua máquina

npm -v

Caso não tenhas ambos em sua máquina, clique aqui para saber como instalar. OBS: Ao instalar o node, ele instalará o npm automaticamente.
Instalar o Angular cli

Logo após verificar o se o node e npm estão instalados, vamos instalar o angular-cli Para saber como instala-ló, clique aqui para mais detalhes
Baixando e configurando o projeto

Clone o repositorio com:

git clone https://arikardnoir@bitbucket.org/arikardnoir/reconhecimentoapp.git

•	Após clonar o projeto para a sua máquina, navegue até a pasta do projeto, e na raíz do mesmo execute o seguinte comando:

npm install

instala todas as dependências necessárias para executar o projeto em sua máquina
Executando o projeto

Os dois comandos abaixo têm a função de subir o servidor para poder executar o projeto. O primeiro comando, sobe o servidor e inicializa o projecto no seu browser automaticamente, já o segundo apenas sobe o servidor, para acessa-ló basta colar a seguinte url: http://localhost:4200/.

ng serve -o

ou

ng serve

•	Caso a porta '4200' esteja sendo usada por um outro software, faça o seguinte:

ng serve --port numero_da_porta

exemplo:

ng serve --port 1234
